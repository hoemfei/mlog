package main

import (
	"context"
	"os"
	"os/signal"
	"syscall"
)

func main(){
	sigs, done := make(chan os.Signal, 1), make(chan struct{}, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	ctx, cancel := context.WithCancel(context.Background())
	Init(ctx)
	go func() {
		sugarLogger.Warnf("接收到退出信号: %v, 程序结束运行",  <- sigs)
		done <- struct{}{}
	}()
	//处理监听文件
	for i, k := 0, len(FileNames.Path); i < k; i++ {
		go ReadPathToChan(ctx, FileNames.Path[i].FilePath.Filename, FileNames.Path[i].FilePath.DeviceName, FileNames.Path[i].FilePath.DeviceType, FileNames.Path[i].FilePath.IsOk, FileNames.Path[i].FilePath.RMonitorStr)
	}
	//处理syslog流
	go ReadSyslogToChan()
	//是否启用批量写入es
	if FileNames.EsPerformance.IsOk {
		go saveESRoute(ctx)
	} else {
		go saveEsOne(ctx)
	}
	//过滤处理告警函数
	go handleWe(ctx)
	<- done
	cancel()
}
package main

import (
	"context"
	"fmt"
	"time"
)

func handleWe(ctx context.Context){
	mesBuffer := &EsStruct{}
	for {
		select {
		case mesBuffer = <- wechChan:
				/*
					反向匹配表, 如果在反向匹配表里, 返回true 重新来一轮，读取下一条
					没在反向匹配表里, 返回false, 进入正向匹配表
					正向匹配表, 如果在正向匹配表里, 返回false 进入插件信息优化
					没在正向匹配表里, 返回true, 重新来一轮, 读取下一条
				 */
				stratTime, weatchTime := time.Now().Format("2006-01-02 15:04:05"), time.Now().UTC().Format(time.RFC3339)
				if ReverseTable(mesBuffer.Datalog) {			//反向匹配, 匹配到了返回true, 处理下一条信息
				} else if PositiveTable(mesBuffer.Datalog) {	//正向匹配, 匹配到了返回false, 往下走
				} else if mes, ok := InterfaceStatus(mesBuffer.Datalog); ok {		//Interface 接口告警信息优化
					mes = fmt.Sprintf("%s%s\n%s: %s 日志告警\n%s\n发生时间: %s", FileNames.AddressInfo, Tianqi, mesBuffer.Devicetype, mesBuffer.Devicename, mes, stratTime)
					SendWechatMes(mes)
					AddWechatDoc(aliasIndex1, &EsWechatStruct{Time: weatchTime, Wechat: mes})
				} else if mes, ok = NetModStatus(mesBuffer.Datalog); ok {			//光模块告警信息优化
					mes = fmt.Sprintf("%s%s\n%s: %s 日志告警\n%s\n发生时间: %s", FileNames.AddressInfo, Tianqi, mesBuffer.Devicetype, mesBuffer.Devicename, mes, stratTime)
					SendWechatMes(mes)
					AddWechatDoc(aliasIndex1, &EsWechatStruct{Time: weatchTime, Wechat: mes})
				} else {
					mes = fmt.Sprintf("%s%s\n%s: %s 日志告警⚠\n%s\n发生时间: %s", FileNames.AddressInfo, Tianqi, mesBuffer.Devicetype, mesBuffer.Devicename, mesBuffer.Datalog, stratTime)
					SendWechatMes(mes)
					AddWechatDoc(aliasIndex1, &EsWechatStruct{Time: weatchTime, Wechat: mes})
				}
		case <- ctx.Done():
			return
		}
	}
}

/*
	反向匹配
	正向匹配
 */

func ReverseTable(mes string) bool {
	if len(Rrexp) == 0 {return false}
	for i, k:=0, len(Rrexp); i < k; i++ {
		if Rrexp[i].MatchString(mes) {
			return true
		}
	}
	return false
}
func PositiveTable(mes string) bool {
	if len(Mrexp) == 0 {return true}
	for i, k:=0, len(Mrexp); i < k; i++ {
		if Mrexp[i].MatchString(mes) {
			return false
		}
	}
	return true
}
package main

import (
	"gopkg.in/mcuadros/go-syslog.v2"
	"time"
)

func ReadSyslogToChan() {
	if ! FileNames.EsSyslogConf.Switch {
		return
	}
	channel := make(syslog.LogPartsChannel)
	handler := syslog.NewChannelHandler(channel)
	server := syslog.NewServer()
	server.SetFormat(syslog.Automatic)
	server.SetHandler(handler)
	server.ListenUDP(FileNames.EsSyslogConf.ListenUDP)
	server.Boot()
	go func(channel syslog.LogPartsChannel) {
		if FileNames.EsSyslogConf.IsOk {
			for logParts := range channel {
				tmp := &EsStruct{
					Time: time.Now().UTC().Format(time.RFC3339),
					Address:    FileNames.AddressInfo,
					Devicetype: logParts["hostname"].(string),
					Devicename: logParts["client"].(string),
					Datalog: logParts["content"].(string),
				}
				mesChan <- tmp
				wechChan <- tmp
			}
		} else {
			for logParts := range channel {
				mesChan <- &EsStruct{
					Time: time.Now().UTC().Format(time.RFC3339),
					Address:    FileNames.AddressInfo,
					Devicetype: logParts["hostname"].(string),
					Devicename: logParts["client"].(string),
					Datalog: logParts["content"].(string),
				}
			}
		}
	}(channel)
	server.Wait()
}
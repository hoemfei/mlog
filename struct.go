package main

type EsStruct struct {
	Time    	string   `json:"time"`
	Address 	string   `json:"Address"`
	Devicetype 	string   `json:"DeviceType"`
	Devicename 	string   `json:"DeviceName"`
	Datalog     string   `json:"DataLog"`
}
type EsWechatStruct struct {
	Time		string	`json:"time"`
	Wechat 		string	`json:"wechat"`
}

type Resource struct{
	AddressInfo 	string      	`yaml:"AddressInfo"`
	AddressWeather  string      	`yaml:"AddressWeather"`
	WeatherCode		string
	Path 			[]File          `yaml:"Path"`
	Wechat   		[]WechatServer  `yaml:"wechat"`
	MonitorStr  	[]string    	`yaml:"MonitorStr"`
	RMonitorStr 	[]string    	`yaml:"RMonitorStr"`
	Elasticsearch 	ESconf    		`yaml:"Elasticsearch"`
	EsPerformance	Performance     `yaml:"Performance"`
	EsSyslogConf	SyslogConf		`yaml:"SyslogConf"`
	OutConfigLog	ConfigLog		`yaml:"ConfigLog"`
}

type ConfigLog	struct {
	LogName			string			`yaml:"logName"`
	MaxSize			int				`yaml:"maxSize"`
	MaxBackups		int				`yaml:"maxBackups"`
	MaxAges			int				`yaml:"maxAges"`
	Compress		bool			`yaml:"compress"`
}

type Performance struct {
	IsOk			bool	`yaml:"isOk"`
	BufferSize		int		`yaml:"bufferSize"`
	BufferTimeout   int		`yaml:"bufferTimeout"`
	BufferSlot		int		`yaml:"bufferSlot"`
}

type SyslogConf struct {
	Switch			bool	`yaml:"switch"`
	IsOk			bool	`yaml:"isOk"`
	ListenUDP		string	`yaml:"listenUDP"`
}

type ESconf struct {
	Address 	string  `yaml:"address"`
	User		string	`yaml:"user"`
	Password	string	`yaml:"password"`
	Index   	string  `yaml:"index"`
	Wechat  	string  `yaml:"wechat"`
	Shards  	int		`yaml:"shards"`
	Replicas 	int		`yaml:"replicas"`
}

type File struct {
	FilePath 	FileInfo `yaml:"filePath"`
}

type FileInfo struct {
	Filename 		string      `yaml:"filename"`
	DeviceName 		string   	`yaml:"deviceName"`
	DeviceType 		string   	`yaml:"devicetype"`
	IsOk			bool		`yaml:"isOk"`
	RMonitorStr 	[]string   	`yaml:"RmonitorStr"`
}

type WechatServer struct {
	ConfWechat ConfWe    `yaml:"conf"`
}

type ConfWe struct {
	ToUser  	string      `yaml:"touser"`
	ToParty 	string      `yaml:"toparty"`
	AgentId 	int         `yaml:"agentid"`
	CorpId  	string      `yaml:"corpid"`
	CorpSecret 	string   	`yaml:"corpsecret"`
	wechatToken string
}

type EsWechat struct {
	Time    	string      `json:"time"`
	Regexp 		string      `json:"Regexp"`
	Message     string  	`json:"Message"`
}

//天气预报的结构体
type Weather struct {
	Status 		string 		`json:"status"`
	Count		string		`json:"count"`
	Info 		string		`json:"info"`
	Infocode	string		`json:"infocode"`
	Forecasts 	[]Forecast	`json:"forecasts"`
}
type Forecast struct {
	City 		string		`json:"city"`
	Adcode		string		`json:"adcode"`
	Province	string		`json:"province"`
	Reporttime	string		`json:"reporttime"`
	Casts		[]Cast		`json:"casts"`
}
type Cast struct {
	Date 			string		`json:"date"`
	Week 			string		`json:"week"`
	Dayweather 		string		`json:"dayweather"`
	Nightweather 	string		`json:"nightweather"`
	Daytemp			string		`json:"daytemp"`
	Nighttemp		string		`json:"nighttemp"`
	Daywind			string		`json:"daywind"`
	Nightwind 		string		`json:"nightwind"`
	Daypower 		string		`json:"daypower"`
	Nightpower		string		`json:"nightpower"`
}
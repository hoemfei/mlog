package main

import (
	"context"
	"fmt"
	"github.com/olivere/elastic/v7"
	"time"
)

//添加文档
func AddDoc(esIndex string, esBody *EsStruct) {
	_, err := esCli.Index().
		Index(esIndex).
		BodyJson(esBody).
		Do(context.Background())
	if err != nil {
		sugarLogger.Debug(esBody, "入库失败")
	}
}
//添加wechat文档
func AddWechatDoc(esIndex string, esBody *EsWechatStruct) {
	_, err := esCli.Index().
		Index(esIndex).
		BodyJson(esBody).
		Do(context.Background())
	if err != nil {
		sugarLogger.Debug(esBody, "入库失败")
	}
}

//批量添加
func AddDocBulk(esIndex string, esBody []*EsStruct) {
	bulkRequest := esCli.Bulk()
	for i, k := 0, len(esBody); i<k; i++ {
		bulkRequest = bulkRequest.Add(elastic.NewBulkCreateRequest().Index(esIndex).Doc(esBody[i]))
	}
	_, err := bulkRequest.Do(context.Background())
	if err != nil {
		sugarLogger.Error("Bulk批量提交失败: ", err)
	}
}

//创建索引, 如果索引存在则返回
func CreateIndex(esIndex string, numShards, numReplicas int) {
	exists, err := esCli.IndexExists(fmt.Sprintf("%s-%s-000001",esIndex, time.Now().Format("2006.01.02"))).Do(context.Background())
	//%3Cmy-index-%7Bnow%2Fd%7D-000001%3E
	if err != nil {
		sugarLogger.Panic("检索索引失败: ", err)
	}
	if !exists {
		mapping := map[string]interface{} {
			"settings": map[string]interface{} {
				"number_of_shards": numShards,
				"number_of_replicas": numReplicas,
			},
			"aliases": map[string]interface{} {
				esIndex+"_alias": map[string]bool {
					"is_write_index" : true,
				},
			},
		}
		_, err = esCli.CreateIndex(fmt.Sprintf("%s-%s-000001",esIndex, time.Now().Format("2006.01.02"))).BodyJson(mapping).Do(context.Background())
		if err != nil {
			sugarLogger.Panic("创建索引: ", esIndex, "失败, err: ", err)
		}
		return
	}
	sugarLogger.Info("索引: ", esIndex, " 已存在")
}

//删除索引
func DeleteIndex(esIndex string) {
	_, err := esCli.DeleteIndex(esIndex).Do(context.Background())
	if err != nil {
		sugarLogger.Errorf("索引: %s 删除失败, err: %s", esIndex, err)
	}
}

//删除文档
func DeleteDoc(esIndex, id string) {
	_, err := esCli.Delete().Index(esIndex).Do(context.Background())
	if err != nil {
		sugarLogger.Debugf("删除文档失败: %s", err)
	}
}
package main

import (
	"fmt"
	"regexp"
)

/*
	Interface 接口告警信息优化
 */
var (
	InterfacePlugRegexp 	= regexp.MustCompile(`([Ii]nterface).*([uU][Pp]|[Dd][Oo][Ww][nN])`)
	InterfacePlugEnsName 	= regexp.MustCompile(`[Ii]nterface (.*) has`)
	InterfacePlugEnsStatus	= regexp.MustCompile(` (UP|DOWN) state`)
)
func InterfaceStatus(mes string) (string, bool) {
	if InterfacePlugRegexp.MatchString(mes) {
		Names := InterfacePlugEnsName.FindStringSubmatch(mes)
		Status := InterfacePlugEnsStatus.FindStringSubmatch(mes)
		if len(Names) < 2 || len(Status) < 2 {
			mes = fmt.Sprintf("信息优化失败🚫, 源告警信息为:\n%s", mes)
		} else if Status[1] == "DOWN" {
			mes = fmt.Sprintf("接口: %s 处于 %s ❌ 状态", Names[1], Status[1])
		} else {
			mes = fmt.Sprintf("接口: %s 处于 %s ✅ 状态", Names[1], Status[1])
		}
		return mes, true
	}
	return "", false
}

/*
	光模块插件检测,告警信息优化
 */
var (
	NetMod1PlugRegexp 	= regexp.MustCompile(`err.*The optical power`)
	NetMod1PlugName 	= regexp.MustCompile(`PhysicalName="(.*)",`)
	NetMod2PlugRegexp 	= regexp.MustCompile(`err:.*Optical module power`)
	NetMod2RxGigh  		= regexp.MustCompile(`Rx power is too high`)
	NetMod2LowRece 		= regexp.MustCompile(`Overlow receiving power of fabric module`)
	NetMod2Right   		= regexp.MustCompile(`Overhigh receiving power of fabric module`)
	NetMod2EnsName 		= regexp.MustCompile(`EntPhysicalName=(.+?),`)
	NetMod2EnsCurrent 	= regexp.MustCompile(`The current Rx power is (.+?);`)
	NetMod2EnsName1		= regexp.MustCompile(`EntPhysicalName="(.+?)",`)
	NetMod2EnsCurrent1  = regexp.MustCompile(`Now power is (.+?);`)
)
func NetModStatus(mes string) (string, bool) {
	if NetMod1PlugRegexp.MatchString(mes){
		Names := NetMod1PlugName.FindStringSubmatch(mes)
		if len(Names) < 2 {
			mes = fmt.Sprintf("信息优化失败🚫, 源告警信息为:\n%s", mes)
		} else {
			mes = fmt.Sprintf("接口: %s 光功率超过上限警告阈值或低于下限警告阈值❗", Names[1])
		}
		return mes, true
	}
	if NetMod2PlugRegexp.MatchString(mes) {
		if NetMod2RxGigh.MatchString(mes) {
			Names, Curr := NetMod2EnsName.FindStringSubmatch(mes), NetMod2EnsCurrent.FindStringSubmatch(mes)
			if len(Names) < 2 || len(Curr) < 2 {
				mes = fmt.Sprintf("信息优化失败🚫, 源告警信息为:\n%s", mes)
			} else {
				mes = fmt.Sprintf("接口: %s 收光功率过高, 当前接收功率为: %s ❗",Names[1], Curr[1])
			}
			return mes, true
		} else if NetMod2LowRece.MatchString(mes) {
			Names, Curr := NetMod2EnsName1.FindStringSubmatch(mes), NetMod2EnsCurrent1.FindStringSubmatch(mes)
			if len(Names) < 2 || len(Curr) < 2 {
				mes = fmt.Sprintf("信息优化失败🚫, 源告警信息为:\n%s", mes)
			} else {
				mes =  fmt.Sprintf("接口: %s 收光功率过低, 现在功率为: %s ❗",Names[1], Curr[1])
			}
			return mes, true
		} else if NetMod2Right.MatchString(mes) {
			Names, Curr := NetMod2EnsName1.FindStringSubmatch(mes), NetMod2EnsCurrent1.FindStringSubmatch(mes)
			if len(Names) < 2 || len(Curr) < 2 {
				mes = fmt.Sprintf("信息优化失败🚫, 源告警信息为:\n%s", mes)
			} else {
				mes = fmt.Sprintf("接口: %s 收光功率过高, 现在功率为: %s ❗", Names[1], Curr[1])
			}
			return mes, true
		}
	}
	return "", false
}

/*
	TrunkInterface 接口优化告警
*/
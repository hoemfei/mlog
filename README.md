# mlog

#### 介绍

1. 收集日志，并按地区分类送往ES里，并过滤自定义规则微信告警 
2. 支持从本地日志文件收集日志。 
3. 支持从syslog流中收集日志，本机监听514或者其它udp端口，其它机器往本机发送syslog即可 

#### 软件架构 
![输入图片说明](jpg/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20220803165842.jpg)
![](https://images.gitee.com/uploads/images/2021/0803/101918_0be9df50_6507691.png "微信图片_20210803101747.png")
![](https://images.gitee.com/uploads/images/2021/0803/102205_e2223557_6507691.png "微信图片_20210803102140.png")

#### 安装教程
rpm -ivh http://116.66.49.18:8884/squ-cpu/mlog-0.0.1-1.el7.x86_64.rpm 

#### 使用说明  
管理命令:   
    1. systemctl start   mlog    
    2. systemctl stop    mlog   
    3. systemctl restart mlog  
    4. systemctl status  mlog  
    5. systemctl enable  mlog  

总共就一个配置文件, 维护一个配置文件即可  
配置文件: /etc/moniterFile.yaml

```yaml
#### 配置文件详解   
#配置文件遵循 YMAL 语法格式
#为了日志地区分类
AddressInfo: "上海地区"

#地区天气，基于高德地图免费的api做的, （支持写，区，市，县，省但是只能写一个，要么写市，要么写县。。。。)
AddressWeather: "徐汇区"

#连接ES，把日志存到哪个索引里, 索引会自动创建
#但是为了索引能够根据时间滚动, 还需自行创建日期索引
#例如index: "newlogs"
#PUT %3Cnewlogs-%7Bnow%2Fd%7D-000001%3E
#{
#  "aliases": {
#    "newlogs_alias": {
#      "is_write_index":true
#    }
#  }
#}
#则会创建索引名称为: newlogs-YYYY.MM.dd-000001
#后续会往索引别名里写入, 索引别名为: newlogs_alias
#索引的滚动更新策略请自行在kibana里创建 ilm 生命管理周期 (注意 索引别名要填newlogs_alias), 和索引模板
Elasticsearch:
    address: "http://10.161.x.x:19971"       #ES地址
    #user:                                     #ES的用户名 (没有可以注释掉)
    #password:                                 #ES的用户的密码 (没有可以注释掉)
    index: "newlogs"                           #日志落在哪个索引里, 没有则会自动创建
    wechat: "newwechat"                        #微信告警记录落在哪个索引里, 没有则会自动创建
    shards: 3                                  #索引分片数量 (根据自己的实际情况,注意分片数量不要超过es的data节点数量)
    replicas: 1                                #副本数量

#ES写入性能调整, 是否开启批量写入
Performance:
    isOk: true            #true 表示启用批量写入, false表示关闭，不启用批量写入,
    bufferSize: 5000      #buffer容量,  buffer满了bulk批量写入es
    bufferTimeout: 5      #buffer context.WithDeadline为5秒, 无论是否达到bufferSize个, 都会调用es的bulk将buffer批量写入es, 单位为秒
    bufferSlot: 5         #同时并发几个buffer槽, 用于并发bulk提交


#要监控的syslog输入流, 需要在本机监听udp端口, 其它设备往此程序监听的udp端口发送syslog即可
SyslogConf:
    switch: true               #是否启用从syslog流中读取
    listenUDP: "0.0.0.0:514"    #监听本地 dup端口
    isOk: false                 #是否开启告警匹配


#要监控的日志文件完整路径, 支持通配符匹配
Path:
    - filePath:
          filename: /var/log/nginx/*.log      #如果是通配符, 则也会递归匹配子文件夹里的符合的文件
          deviceName: nginx-node1             #告警时设备的名字
          devicetype: web                     #设备类型
          isOk: true                          #是否开启告警匹配, 产生日志量大的不建议开启(例如每秒1w+, 告警处理不过来)
          RmonitorStr:                        #匹配以下规则,则忽略告警, 支持正则表达式(正则表达式, 须要用单引号包括)
             - '[Ee][Rr]{2}[Oo][Rr]'
             - 'False'
    - filePath:
        filename: /var/log/squid/access*.log
        deviceName: cache-01
        devicetype: CDN服务器
        isOk: false                      
        RmonitorStr:

#监控所有日志, 发现有以下字眼的, 微信告警, 支持正则表达式(正则表达式, 须要用单引号包括)
MonitorStr:
  - ' UP '
  - ' DOWN '
  - 'loopback detection status: 3'
  - 'shutdown'
  - 'err.*The optical power'                     #匹配光模块收光异常
  - 'err.*Optical module power'                  #匹配光模块收光异常
  - '当前状态:(up)|(down)'                       #匹配迪普设备的up down日志
  - '[Ee][Rr]{2}[Oo][Rr]'                         #匹配忽略大小写Error字段
  - 'Drive [0-9]{1,2} is (removed from)|(installed in) disk drive bay'  #匹配磁盘插拔日志
  - 'DIMM_[A_Z]\[0-9]'                                                  #匹配内存损坏日志

#排除匹配, 支持正则表达式(发现这下面的匹配了则不告警, 优先级高于MonitorStr)
RMonitorStr:
  - 'Failed to login'
  - 'warning: The line protocol IP on the interface'
  - 'Slot=[0-9]{1,2},Vcpu=[0-9]{1,2}'

#企业微信配置, 可以配置多个企业微信
wechat:
    - conf:
        touser: WangFei                   #企业号中的用户帐号, 配置不正常, 将按部门发送。
        toparty: 2                        #企业号中的部门id。
        agentid: 1000007                  #企业号中的应用id。
        corpid: ww89xxxxxxxxxxxa2d        #企业号的标识
        corpsecret: wpjyFBnIxxxxxxxxxxxxxxxxxxxBt89tLzm30   #企业号中的应用的Secret

#日志选项默认即可
ConfigLog:
  logName: '/var/log/mlog.log'         #日志输出文件
  maxSize: 100                  #在进行切割之前，日志文件的最大大小（以MB为单位）
  maxBackups: 5                 #保留旧文件的最大个数
  maxAges: 365                  #保留旧文件的最大天数
  compress: false                #是否压缩/归档旧文件
```


  
  